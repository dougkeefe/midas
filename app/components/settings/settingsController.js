(function(){
angular
	.module('midas')
	.controller("SettingsController", SettingsController);

function SettingsController(userData, currencyData) {
	var vm = this;	// init capture variable
	vm.userData = {};
	
	vm.userData = userData.getUserData();
	
	vm.risk = vm.userData.user.risk;
	vm.activePairs = vm.userData.user.tradeablePairs;
	
	vm.currencyData = currencyData.getCurrencyPairs();
	
	vm.isActivePair = function(pair) {
		for(i = 0; i < vm.activePairs.length; i++) {
			if(pair == vm.activePairs[i]) {
				return true;
			}
		}
		
		return false;
	}
	
	vm.toggleTradeStatus = function(pair) {
		// remove if pair exists
		var ind = vm.activePairs.indexOf(pair);
		if(ind > -1) {
			vm.activePairs.splice(ind, 1);
		}
		else {
			vm.activePairs.push(pair);
		}
			 
		// push if pair doesn't
	}
}
})();