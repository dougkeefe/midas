(function() {
'use strict';
angular
	.module('midas')
	.factory('currencyData', currencyData);

function currencyData($resource) {
	var CurrencyData = $resource('shared/data/currency_pairs.json');
	
	function getCurrencyPairs() {
		return {
			"pairs" : 
				[ 
					 {
						"name": "AUDCAD",
						"bid": 1.33213,
						"ask": 1.33542 
					}
					,
					{
						"name": "AUDUSD",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "CHFJPY",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "EURNZD",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "GBPJPY",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "TRYJPY",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "USDSE",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "AUDCHF",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "EURAUD",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "EURSEK",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "GBPNZD",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "USDCAD",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "USDTR",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "AUDJPY",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "EURCAD",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "EURTRY",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "GBPUSD",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "USDCHF",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "USDZA",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "AUDNZD",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "EURCHF",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "EURUSD",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "NZDCAD",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "USDCNH",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "ZARJP",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "EURGBP",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "GBPAUD",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "NZDCHF",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "USDJPY",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "CADCHF",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "EURJPY",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "GBPCAD",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "NZDJPY",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "USDMXN",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "CADJPY",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "EURNOK",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "GBPCHF",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "NZDUSD",
						"bid": 1.33213,
						"ask": 1.3542
					},
					{
						"name": "USDNO",
						"bid": 1.33213,
						"ask": 1.3542
					}
				]
		};
	}
	return {
		getCurrencyPairs: getCurrencyPairs
	};
}
})();
