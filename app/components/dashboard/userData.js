(function() {
'use strict';
angular
	.module('midas')
	.factory('userData', userData);

function userData($resource) {
	var UserData = $resource('shared/data/users.json');

	function getUserData() {
		return {
			"user" : {
				"username" : "dougkeefe",
				"name" : "Doug Keefe",
				"email" : "doug.keefe@gmail.com",
				"mobile" : "902-314-1836",
				"balance" : 115272.12,
				"investedAmount" : 95652.27,
				"risk" : 50,
				"trading" : true,
				"tradeablePairs" : [
					"USDCAD",
					"EURUSD",
					"GBPUSD",
					"EURCHF"
				],
				"activeTrades" : [
					{
						pair: "CADUSD",
						entryDate: "2016-04-02 11:23:11",
						entryPrice: 1.11244,
						investType: "long",
						lotSize: 10000,
						currentPrice: 1.22152
					},
					{
						pair: "EURUSD",
						entryDate: "2016-04-04 09:33:41",
						entryPrice: 1.16234,
						investType: "short",
						lotSize: 8000,
						currentPrice: 1.22152
					},
				],
				"netPerformace" : 52362.12,
				"forexComEmail" : "doug.keefe@gmail.com"
				
			},
			"userHistory" : {
				"username" : "dougkeefe",
				"range" : "today",
				"netPerformance": 123562.43,
				"trades" : 
					[
					  {
						  pair: "EURUSD",
						  entryPrice: 1.22431,
						  entryDate: "2016-04-02 11:23:11",
						  exitPrice: 1.25634,
						  exitDate: "2016-04-02 11:23:11",
						  type: "long",
						  lotSize: 10000
					  },
					  {
						  pair: "EURUSD",
						  entryPrice: 1.22431,
						  entryDate: "2016-04-02 11:23:11",
						  exitPrice: 1.25634,
						  exitDate: "2016-04-02 11:23:11",
						  type: "long",
						  lotSize: 10000
					  },
					  {
						  pair: "USDJPY",
						  entryPrice: 221.22,
						  entryDate: "2016-04-02 11:23:11",
						  exitPrice: 221.22,
						  exitDate: "2016-04-02 11:23:11",
						  type: "long",
						  lotSize: 10000
					  },
					  {
						  pair: "USDGBP",
						  entryPrice: 1.22431,
						  entryDate: "2016-04-02 11:23:11",
						  exitPrice: 1.25634,
						  exitDate: "2016-04-02 11:23:11",
						  type: "short",
						  lotSize: 10000
					  },
					  {
						  pair: "CADUSD",
						  entryPrice: 1.22431,
						  entryDate: "2016-04-02 11:23:11",
						  exitPrice: 1.25634,
						  exitDate: "2016-04-02 11:23:11",
						  type: "short",
						  lotSize: 10000
					  },
					  {
						  pair: "EURGBP",
						  entryPrice: 1.22431,
						  entryDate: "2016-04-02 11:23:11",
						  exitPrice: 1.25634,
						  exitDate: "2016-04-02 11:23:11",
						  type: "long",
						  lotSize: 10000
					  }
					 ]
			}
		};
//		return UserData.query().$promise.then(function(results) {
//			return results;
//		}, function(error){
//			console.log(error);
//		});
	}
	return {
		getUserData: getUserData
	};
}
})();