(function(){
angular
	.module("midas")
	.config(function($stateProvider, $urlRouterProvider) {
	    
	    $urlRouterProvider.otherwise('dashboard');
	    
	    $stateProvider
	        // HOME STATES ========================================
	        .state('dashboard', {
	            url: '/dashboard',
	            template: getDashboardTemplate(),
	            controller: 'DashboardController as dashboardCtrl'
	            //templateUrl: 'app/components/dashboard/dashboardView.html'
	        })

	        
	        // SETTINGS PAGE =================================
	        .state('settings', {
	           url: '/settings',
	           template: getSettingsTemplate(),
	           controller: 'SettingsController as settingsCtrl',
	           //templateUrl: 'app/components/settings/settingsView.html'
	        });
	});

function getSettingsTemplate() {
	return `
	<div class="row">
    <div class="col-xs-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <div class="title"><i class="fa fa-gear"></i> System Settings</div>
                    <div class="description"></div>
                </div>
               
            </div>

            <div class="card-body">
                <div class="row no-margin">
                    <div class="col-sm-6">
                        <div class="card primary">
                            <div class="card-body">
                                <h4>Risk Percentage <i class="fa fa-question-circle" style="color:#B9B9B9;"></i></h4>
                                <input type="range" min="1" max="75" ng-model="settingsCtrl.risk" value ="{{ settingsCtrl.userData.user.risk }}" step="1" />
                                <span id="riskRange">{{ settingsCtrl.risk }}</span>
                            </div>
                        </div>
                        <div class="card primary">
                            <div class="card-body">
                                <h4>Tradeable Pairs <i class="fa fa-question-circle" style="color:#B9B9B9;"></i></h4>
                                <span ng-repeat="pair in settingsCtrl.currencyData.pairs">
                                	<button type="button" class="btn" ng-class="{ 'btn-primary' : settingsCtrl.isActivePair(pair.name) }" ng-click="settingsCtrl.toggleTradeStatus(pair.name)"><i class="fa fa-check" ng-show="settingsCtrl.isActivePair(pair.name)"></i> {{ pair.name }}</button>
                                </span>
                            </div>
                        </div>
                        <div class="card primary">
                            <div class="card-body">
                                <h4><i class="fa fa-gear"></i> System Settings</h4>
                                <p>
                                    <label for="notificationName">Name</label>
                                    <input type="email" class="form-control" id="notificationName" name="notificationName" placeholder="Enter full name" ng-model="settingsCtrl.userData.user.name">
                                </p>

                                <p>
                                    <label for="notificationEmail">Email address</label>
                                    <input type="email" class="form-control" id="notificationEmail" name="notificationEmail" placeholder="Enter email" ng-model="settingsCtrl.userData.user.email">
                                </p>
                                <p>
                                    <label for="notificationSMS">Mobile Phone #</label>
                                    <input type="text" class="form-control" id="notificationSMS" name="notificationSMS" placeholder="(###) ###-####" ng-model="settingsCtrl.userData.user.mobile">
                                </p>

                                <fieldset>
                                    <legend style="font-size: 12pt; padding-top:25px;">Update Password</legend>
                                    <p>
                                        <label for="accountPassword">New Password</label>
                                        <input type="password" class="form-control" id="accountPassword" name="accountPassword" >
                                    </p>
                                    <p>
                                        <label for="accountPasswordConfirm">Confirm Password</label>
                                        <input type="password" class="form-control" id="accountPasswordConfirm" name="accountPasswordConfirm" >
                                    </p>    
                                </fieldset>
                                
                            </div>
                        </div>
                        <div class="card primary">
                            <div class="card-body">
                                <h4><i class="fa fa-money"></i> FOREX.com Account Information</h4>
                                <p>
                                    <label for="forexEmail">Email address</label>
                                    <input type="email" class="form-control" id="forexEmail" name="forexEmail" placeholder="Enter email" ng-model="settingsCtrl.userData.user.forexComEmail">
                                </p>
                                <p>
                                    <label for="forexPassword">Password</label>
                                    <input type="password" class="form-control" id="forexPassword" name="forexPassword">
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-jumbotron">
                                <h1>Help is Here</h1>
                                <p>Need help customizing your settings?  Here are the best practices most of our clients are following to get the most of out MIDAS. </p>
                            </div>
                            <div class="card-body">
                                <button type="button" class="btn btn-lg btn-success"><i class="fa fa-save"></i> Save Settings</button>
                                <button type="button" class="btn btn-lg btn-default"><i class="fa fa-refresh"></i> Reset</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>
`;

}


function getDashboardTemplate() {
	return `

	<div class="row" >
		<div class="col-lg-5 col-md-6">
			<div class="row">
			<div class="col-xs-12 no-padding">
			<div class="card">
				<div class="card-header">
					<div class="card-title">
						<div class="title">
							<i class="fa fa-bank"></i> Account Overview
						</div>
						<div class="description"></div>
					</div>
		
				</div>
		
				<div class="card-body" style="padding-bottom: 0">
					<div class="row">
						<div class="col-xs-12" style="padding-bottom: 0px;">
							<ul class="list-group" style="margin-bottom: 0">
								<li class="list-group-item">Account Balance: <span
									class="badge" style="color: #000; background: #fff;">{{ dashboardCtrl.userData.user.balance | currency }}</span>
								</li>
								<li class="list-group-item">Invested: <span class="badge"
									style="color: #000; background: #fff;">{{ dashboardCtrl.userData.user.investedAmount | currency }}</span>
								</li>
								<li class="list-group-item">Cash: <span class="badge"
									style="color: #000; background: #fff;">{{ dashboardCtrl.userData.user.balance - dashboardCtrl.userData.user.investedAmount | currency }}</span>
								</li>
								<li class="list-group-item"
									style="height: 50px; line-height: 30px;">Trading Status: {{ dashboardCtrl.userData.user.trading }}
									<span class="badge" style="color: #000; background: #fff;"><input
										type="checkbox" class="toggle-checkbox"
										name="hstry-invest-type" id="hstry-invest-type"
										data-on-text="ON" data-on-color="success" data-off-text="OFF"
										data-off-color="danger" checked>
								</span>
								</li>
							</ul>
						</div>
						<div class="col-lg-6">
							<!--
					                                   <canvas id="pie-chart" class="chart" width="2" height="285" style="width: 2px; height: 285;"></canvas>
					-->
						</div>
					</div>
				</div>
			</div>
		</div>
			</div>
			<div class="row">
				<div class="card">
					<div class="card-header">
						<div class="card-title">
							<div class="title">
								<i class="fa fa-history"></i> Trade History
							</div>
							<div class="description"></div>
						</div>
						<div class="pull-right card-action">
							<div class="btn-group" role="group" aria-label="...">
								Date Range <select>
									<option value="AK">Today</option>
									<option value="HI">Week</option>
									<option value="SC">Month</option>
									<option value="VT">YTD</option>
									<option value="VA">Year</option>
									<option value="WV">Custom Date</option>
								</select>
							</div>
						</div>
					</div>
					<div class="card-body no-padding">
						<table class="table table-hover table-condensed">
							<thead>
								<tr>
									<th></th>
									<th>Entry <i class="fa fa-usd"></i>
									</th>
									<th>Entry <i class="fa fa-clock-o"></i>
									</th>
									<th>Exit <i class="fa fa-usd"></i>
									</th>
									<th>Exit <i class="fa fa-clock-o"></i>
									</th>
									<th>Size</th>
									<th>Type</th>
									<th>P/L <i class="fa fa-usd"></i>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="trade in dashboardCtrl.userData.userHistory.trades">
									<td>{{ trade.pair }}</td>
									<td ng-class="{ 'success' : trade.type == 'long', 'danger' : trade.type == 'short' }">$1.12432</td>
									<td ng-class="{ 'success' : trade.type == 'long', 'danger' : trade.type == 'short' }">2016-03-11 03:45:40</td>
									<td ng-class="{ 'success' : trade.type == 'long', 'danger' : trade.type == 'short' }">$1.16432</td>
									<td ng-class="{ 'success' : trade.type == 'long', 'danger' : trade.type == 'short' }">2016-03-11 03:45:40</td>
									<td ng-class="{ 'success' : trade.type == 'long', 'danger' : trade.type == 'short' }">10,000</td>
									<td ng-class="{ 'success' : trade.type == 'long', 'danger' : trade.type == 'short' }">Buy</td>
									<td ng-class="{ 'success' : trade.type == 'long', 'danger' : trade.type == 'short' }">$4,000.00</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>


			<div class="row">
				<div class="card">
					<div class="card-header" style="border-top: 1px grey solid;">
						<div class="card-title">
							<div class="row">

								<div
									class="col-lg-8 col-md-8 col-sm-8 col-xs-8 text-right title text-right">Total
									Realized Profit / Loss</div>
								<div
									class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-right title text-right">{{ dashboardCtrl.userData.userHistory.netPerformance | currency }}</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-7 col-md-6">

			<div class="row">
				<div class="col-xs-12">
					<div class="card">
						<div class="card-header">
							<div class="card-title">
								<div class="title">Current Positions</div>
							</div>
							<div class="pull-right card-action">
								<button type="button" class="btn btn-danger" ng-click="dashboardCtrl.liquidatePositions();">Liquidate Positions</button>
							</div>
						</div>

						<div class="card-body no-padding">
						  <uib-tabset active="active">							    
							    <uib-tab index="$index + 1" ng-repeat="pair in dashboardCtrl.userData.user.activeTrades" heading="{{ pair.pair }}" active="pair.active">
								    <div class="row">
										<div class="col-lg-2 col-md-6 col-sm-6 col-xs-6">
											Investment Type:</div>
										<div class="col-lg-10 col-md-6 col-sm-6 col-xs-6">
											{{ pair.investType }}
										</div>
									</div>
									<div class="row">
										<div class="col-lg-2 col-md-6 col-sm-6 col-xs-6 ">Entry
											Date / Time:</div>
										<div class="col-lg-10 col-md-6 col-sm-6 col-xs-6 ">
											<strong>{{ pair.entryDate | date:short }}</strong>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-2 col-md-6 col-sm-6 col-xs-6 ">Entry
											Price:</div>
										<div class="col-lg-10 ol-md-6 col-sm-6 col-xs-6 ">
											<strong>$ {{ pair.entryPrice | number:5 }}</strong>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-2 col-md-6 col-sm-6 col-xs-6 ">Lot
											Size:</div>
										<div class="col-lg-10 col-md-6 col-sm-6 col-xs-6 ">
											<strong>{{ pair.lotSize }}</strong>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-2 col-md-6 col-sm-6 col-xs-6 ">
											Realized Profit / Loss:</div>
										<div class="col-lg-10 col-md-6 col-sm-6 col-xs-6 "
											style="font-weight: bold;">
											<strong>{{ pair.lotSize * (pair.currentPrice - pair.entryPrice) | currency }}</strong>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-2 col-md-6 col-sm-6 col-xs-6 ">
											<button type="button" class="btn btn-warning" ng-click="dashboardCtrl.liquidatePosition(pair.pair)">Exit
												Position</button>
										</div>
									</div>
							    </uib-tab>
						  </uib-tabset>
						</div>
						<div class="card-header" style="border-top: 1px grey solid;">
							<div class="card-title row">

								<div class="col-lg-10 col-md-10 col-sm-10 col-xs-8 title">Total
									Unrealized Profit / Loss</div>
								<div
									class="col-lg-2 col-md-2 col-sm-2 col-xs-4 text-right title text-right"
									style="color: green;">{{ dashboardCtrl.userData.user.netPerformace | currency }}</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>





	<!-- MODALS -->
	 <script type="text/ng-template" id="myModalContent.html">
     <div class="modal-header">
         <h3 class="modal-title">I'm a modal!</h3>
     </div>
     <div class="modal-body">
         <ul>
             <li ng-repeat="item in items">
                 <a href="#" ng-click="$event.preventDefault(); selected.item = item">{{ item }}</a>
             </li>
         </ul>
         Selected: <b>{{ selected.item }}</b>
     </div>
     <div class="modal-footer">
         <button class="btn btn-primary" type="button" ng-click="ok()">OK</button>
         <button class="btn btn-warning" type="button" ng-click="cancel()">Cancel</button>
     </div>
 </script>
	
	<div class="modal fade modal-danger" id="modalLiquidate" tabindex="-1"
		role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">x</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Exit Position</h4>
				</div>
				<div class="modal-body">You are about to liquidate your
					position in the GBP/USD pair. Are you sure?</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-danger">Confirm</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade modal-danger" id="modalLiquidateAll"
		tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
		aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">x</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Liquidate All
						Positions</h4>
				</div>
				<div class="modal-body">You are about to liquidate ALL of your
					positions. Are you sure?</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-danger">Confirm</button>
				</div>
			</div>
		</div>
	</div>
`;
}
})();