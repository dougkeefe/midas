(function(){
angular
	.module('midas')
	.controller('NavbarController', NavbarController);

function NavbarController($location){
	var vm = this;	//capture variable
	
	vm.isActive = function(viewLocation) {
		return viewLocation === $location.path();
	}
	
	vm.toggled = function(open) {
    console.log('Dropdown is now: ', open);
  };

}
})();