(function(){
angular
	.module('midas')
	.directive("midasNavBar", NavBar);

function NavBar() {
	return {
		restrict: 'E',
		template: getTemplate(),
		controller: 'NavbarController as navbarCtrl'
	};
}

function getTemplate() {
	return `
    <nav class="navbar navbar-default navbar-fixed-top navbar-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-expand-toggle">
                <i class="fa fa-bars icon"></i>
            </button>
            <ol class="breadcrumb navbar-breadcrumb">
                <li class="active">MIDAS Dashboard</li>
            </ol>
            <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
                <i class="fa fa-th icon"></i>
            </button>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
                <i class="fa fa-times icon"></i>
            </button>
            <li class="dropdown">
                <span class="dropdown-toggle" style="color: #fff; line-height:60px;font-family: 'Roboto Condensed', sans-serif;  padding-left:5px;padding-right:5px;">Auto Trading

                <input type="checkbox" class="toggle-checkbox" name="hstry-invest-type" id="hstry-invest-type" data-on-text="ON" data-on-color="success" data-off-text="OFF" data-off-color="danger" checked>
                </span>
                
            </li>
             

            <li class="profile" uib-dropdown on-toggle="navbarCtrl.dropdownToggled(open)">
					<a href="#" id="simple-dropdown" uib-dropdown-toggle>Doug Keefe<span class="caret"></span></a>
					<ul class="dropdown-menu animated fadeInDown" uib-dropdown-menu aria-labelledby="simple-dropdown">
						<li>
							<div class="profile-info">
								<h4 class="username">Doug Keefe</h4>
								<p>doug.keefe@gmail.com</p>
								<div class="btn-group margin-bottom-2x" role="group">
									<a ui-sref="settings"><button type="button" class="btn btn-default"><i class="fa fa-gear"></i> Settings</button></a>
									<a href="login.html"><button type="button" class="btn btn-default"><i class="fa fa-sign-out"></i> Logout</button></a>
								</div>
							</div>
						</li>
					</ul>
            </li>
        </ul>
    </div>
</nav>
<div class="side-menu sidebar-inverse">
    <nav class="navbar navbar-default" role="navigation">
        <div class="side-menu-container">
            <div class="navbar-header">
                <a class="navbar-brand" ui-sref="#">
                    <div class="icon fa fa-line-chart"></div>
                    <div class="title">MIDAS Forex V1.0</div>
                </a>
                <button type="button" class="navbar-expand-toggle pull-right visible-xs">
                    <i class="fa fa-times icon"></i>
                </button>
            </div>
            <ul class="nav navbar-nav">
                <li ng-class="{ active: navbarCtrl.isActive('/dashboard') }">
                    <a ui-sref="dashboard">
                        <span class="icon fa fa-tachometer"></span><span class="title">Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="reports.html">
                        <span class="icon fa fa-bar-chart-o"></span><span class="title">Reporting</span>
                    </a>
                </li>
                <li ng-class="{ active: navbarCtrl.isActive('/settings') }">
                    <a ui-sref="settings">
                        <span class="icon fa fa-gear"></span><span class="title">Settings</span>
                    </a>
                </li>
                <li>
                    <a href="support.html">
                        <span class="icon fa fa-question-circle"></span><span class="title">Support</span>
                    </a>
                </li>
                <li>
                    <a href="login.html">
                        <span class="icon fa fa-sign-out"></span><span class="title">Log Out</span>
                    </a>
                </li>
                
                
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
</div>
`;
}
})();